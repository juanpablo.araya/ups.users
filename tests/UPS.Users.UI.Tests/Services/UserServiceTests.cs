﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using Moq.AutoMock;
using Shouldly;
using UPS.Users.UI.Models;
using UPS.Users.UI.Repositories.Abstractions;
using UPS.Users.UI.Services;
using UPS.Users.UI.Services.Abstractions;
using Xunit;

namespace UPS.Users.UI.Tests.Services
{
    public class UserServiceTests
    {
        [Fact]
        public void GetUsers_ShouldCallRepository()
        {
            // Arrange
            const int page = 10;
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<UserService>();

            // Act
            service.GetUsers(page);

            // Assert
            mocker
                .GetMock<IUserRepository>()
                .Verify(repository => repository.GetUsers(page));
        }

        [Theory]
        [InlineData("   name   ", "    Bruce", null)]
        [InlineData("email       ", "batman   ", 2)]
        [InlineData("        gender", "      Male", 1)]
        [InlineData("status ", "   Active    ", 5)]
        public async Task FindUsers_ValidParameter_ShouldCallRepository(string property, string value, int? page)
        {
            // Arrange
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<UserService>();

            // Act
            await service.Find(property, value, page);

            // Assert
            mocker
                .GetMock<IUserRepository>()
                .Verify(repository => repository.Find(property.Trim(), value.Trim(), page));
        }

        [Theory]
        [InlineData("age", "100", 1)]
        [InlineData(null, "Male", 2)]
        [InlineData("", "Active", null)]
        [InlineData("        ", null, null)]
        [InlineData(null, null, null)]
        [InlineData("invalidProperty", "invalidValue", 3)]
        public async Task FindUsers_InvalidParameterAndValues_ShouldReturnEmptyArray(string property, string value,
            int? page)
        {
            // Arrange
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<UserService>();

            // Act
            var result = await service.Find(property, value);

            // Assert
            mocker
                .GetMock<IUserRepository>()
                .Verify(repository => repository.Find(It.IsAny<string>(), It.IsAny<string>(), page), Times.Never);
            result.Data.ShouldBeEmpty();
        }

        [Fact]
        public async Task Delete_ShouldCallRepository()
        {
            // Arrange
            var userId = 12;
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<UserService>();

            // Act
            await service.Delete(userId);

            // Assert: Delete is idempotent. It should call the repository, no matter if the id exists or not
            mocker
                .GetMock<IUserRepository>()
                .Verify(repository => repository.Delete(userId), Times.Once);
        }

        [Fact]
        public async Task Get_UserExists_ShouldReturnUser()
        {
            // Arrange
            var userId = 12;
            var user = new User();
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<UserService>();
            var repositoryMock = mocker.GetMock<IUserRepository>();
            repositoryMock
                .Setup(repository => repository.Get(userId))
                .ReturnsAsync(user);

            // Act
            var returnedUser = await service.Get(userId);

            // Assert
            repositoryMock.Verify(repository => repository.Get(userId));
            returnedUser.ShouldBe(user);
        }

        [Fact]
        public async Task Get_UserDoesNotExist_ShouldReturnNull()
        {
            // Arrange
            var userId = 12;
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<UserService>();
            var repositoryMock = mocker.GetMock<IUserRepository>();
            repositoryMock
                .Setup(repository => repository.Get(userId))
                .ReturnsAsync((User) null);

            // Act
            var returnedUser = await service.Get(userId);

            // Assert
            repositoryMock.Verify(repository => repository.Get(userId));
            returnedUser.ShouldBeNull();
        }

        [Fact]
        public async Task Add_InvalidUser_ShouldReturnError()
        {
            // Arrange
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<UserService>() as IUserService;
            var validationServiceMock = mocker.GetMock<IUserValidationService>();
            var user = new User();
            validationServiceMock
                .Setup(validationService =>
                    validationService.IsValid(user, out It.Ref<IEnumerable<ValidationError>>.IsAny))
                .Returns(false);

            // Act
            var result = await service.Add(user);

            // Assert
            validationServiceMock
                .Verify(validationService => validationService.IsValid(
                        user,
                        out It.Ref<IEnumerable<ValidationError>>.IsAny),
                    Times.Once);
            result.Success.ShouldBeFalse();
        }

        [Fact]
        public async Task Add_ValidUser_ShouldSave()
        {
            // Arrange
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<UserService>() as IUserService;
            var validationServiceMock = mocker.GetMock<IUserValidationService>();
            var user = new User
            {
                Email = "the@email.com",
                Gender = "Female",
                Name = "The name",
                Status = "Inactive"
            };
            validationServiceMock
                .Setup(validationService =>
                    validationService.IsValid(user, out It.Ref<IEnumerable<ValidationError>>.IsAny))
                .Returns(true);
            var repositoryMock = mocker.GetMock<IUserRepository>();

            // Act
            var result = await service.Add(user);

            // Assert
            repositoryMock.Verify(repository => repository.Add(user), Times.Once);
            result.Success.ShouldBeTrue();
        }

        [Fact]
        public async Task Update_InvalidUser_ShouldReturnError()
        {
            // Arrange
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<UserService>() as IUserService;
            var validationServiceMock = mocker.GetMock<IUserValidationService>();
            var user = new User();
            validationServiceMock
                .Setup(validationService =>
                    validationService.IsValidForEdit(user, out It.Ref<IEnumerable<ValidationError>>.IsAny))
                .Returns(false);
            var repositoryMock = mocker.GetMock<IUserRepository>();

            // Act
            var result = await service.Update(user);

            // Assert
            result.Success.ShouldBeFalse();
            repositoryMock.Verify(repository => repository.Update(user), Times.Never);
        }

        [Fact]
        public async Task Update_ValidUser_ShouldSave()
        {
            // Arrange
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<UserService>() as IUserService;
            var validationServiceMock = mocker.GetMock<IUserValidationService>();
            var user = GetValidUser();
            validationServiceMock
                .Setup(validationService =>
                    validationService.IsValidForEdit(user, out It.Ref<IEnumerable<ValidationError>>.IsAny))
                .Returns(true);
            var repositoryMock = mocker.GetMock<IUserRepository>();

            // Act
            var result = await service.Update(user);

            // Assert
            repositoryMock.Verify(repository => repository.Update(user), Times.Once);
            result.Success.ShouldBeTrue();
        }

        private static User GetValidUser()
        {
            return new User
            {
                Email = "the@email.com",
                Gender = "Female",
                Name = "The name",
                Status = "Inactive"
            };
        }
    }
}