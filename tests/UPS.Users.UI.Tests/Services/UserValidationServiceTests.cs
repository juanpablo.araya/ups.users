﻿using System;
using System.Collections.Generic;
using System.Linq;
using Shouldly;
using UPS.Users.UI.Models;
using UPS.Users.UI.Services;
using UPS.Users.UI.Services.Abstractions;
using Xunit;

namespace UPS.Users.UI.Tests.Services
{
    public class UserValidationServiceTests
    {
        [Theory]
        [InlineData(null)]
        [InlineData("      ")]
        [InlineData("")]
        public void IsValid_IncorrectName_ShouldShowError(string name)
        {
            // Arrange
            var service = new UserValidationService() as IUserValidationService;
            var user = GetCorrectUser();
            user.Name = name;

            // Act
            var result = service.IsValid(user, out var validationErrors);

            // Assert
            ValidateErrorProperty(result, validationErrors, nameof(User.Name), "Incorrect Name");
        }

        [Theory]
        [InlineData("   ")]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("incorrect_email@.")]
        public void IsValid_IncorrectEmail_ShouldShowError(string email)
        {
            // arrange
            var service = new UserValidationService() as IUserValidationService;
            var user = GetCorrectUser();
            user.Email = email;

            // Act
            var result = service.IsValid(user, out var validationErrors);

            // Assert
            ValidateErrorProperty(result, validationErrors, nameof(User.Email), "Incorrect Email");
        }

        [Theory]
        [InlineData("   ")]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("MaleAndFemale")]
        public void IsValid_IncorrectGender_ShouldShowError(string gender)
        {
            // arrange
            var service = new UserValidationService() as IUserValidationService;
            var user = GetCorrectUser();
            user.Gender = gender;

            // Act
            var result = service.IsValid(user, out var validationErrors);

            // Assert
            ValidateErrorProperty(result, validationErrors, nameof(User.Gender), "Incorrect Gender");
        }

        [Theory]
        [InlineData(" Male  ")]
        [InlineData("  Female")]
        public void IsValid_ValidGender_ValidationShouldBeCorrect(string gender)
        {
            // arrange
            var service = new UserValidationService() as IUserValidationService;
            var user = GetCorrectUser();
            user.Gender = gender;

            // Act
            var result = service.IsValid(user, out var validationErrors);

            // Assert
            result.ShouldBeTrue();
        }

        [Theory]
        [InlineData("   ")]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("ActiveAndInactive")]
        public void IsValid_IncorrectStatus_ShouldShowError(string status)
        {
            // arrange
            var service = new UserValidationService() as IUserValidationService;
            var user = GetCorrectUser();
            user.Status = status;

            // Act
            var result = service.IsValid(user, out var validationErrors);

            // Assert
            ValidateErrorProperty(result, validationErrors, nameof(User.Status), "Incorrect Status");
        }

        [Fact]
        public void IsValid_ValidStatus_ValidationShouldBeCorrect()
        {
            // arrange
            var service = new UserValidationService() as IUserValidationService;
            var user = GetCorrectUser();

            // Act
            var result = service.IsValid(user, out var validationErrors);

            // Assert
            result.ShouldBeTrue();
        }

        [Fact]
        public void IsValidForEdit_InvalidId_ShouldShowError()
        {
            // arrange
            var service = new UserValidationService() as IUserValidationService;
            var user = GetCorrectUser();
            user.Id = 0;

            // Act
            var result = service.IsValidForEdit(user, out var validationErrors);

            // Assert
            ValidateErrorProperty(result, validationErrors, nameof(User.Id), "Incorrect Id");
        }

        private void ValidateErrorProperty(in bool result, IEnumerable<ValidationError> validationErrors,
            string property, string value)
        {
            result.ShouldBe(false);

            validationErrors
                .Where(error => error.Property == property)
                .Any(error => error.Value == value)
                .ShouldBeTrue();
        }

        private static User GetCorrectUser()
        {
            return new User
            {
                Id = 1,
                Email = "the@mail.com",
                Gender = "Male",
                Name = "The name",
                Status = "Active",
                CreatedAt = DateTime.Now.AddDays(-10),
                UpdatedAt = DateTime.Now.AddDays(-5)
            };
        }
    }
}