﻿using System.Threading.Tasks;
using UPS.Users.UI.Models;

namespace UPS.Users.UI.Services.Abstractions
{
    public interface IUserService
    {
        Task<PaginatedResult<User>> GetUsers(int page = 1);
        Task<PaginatedResult<User>> Find(string findBy, string value, int? page = null);
        Task<bool> Delete(int userId);
        Task<User> Get(int userId);
        Task<AddEditResult> Add(User user);
        Task<AddEditResult> Update(User user);
    }
}