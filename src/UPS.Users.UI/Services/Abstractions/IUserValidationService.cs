﻿using System.Collections.Generic;
using UPS.Users.UI.Models;

namespace UPS.Users.UI.Services.Abstractions
{
    public interface IUserValidationService
    {
        bool IsValid(User user, out IEnumerable<ValidationError> validationErrors);
        bool IsValidForEdit(User user, out IEnumerable<ValidationError> validationErrors);
    }
}