﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UPS.Users.UI.Models;
using UPS.Users.UI.Repositories.Abstractions;
using UPS.Users.UI.Services.Abstractions;

namespace UPS.Users.UI.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserValidationService _userValidationService;

        public UserService(IUserRepository userRepository, IUserValidationService userValidationService)
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _userValidationService =
                userValidationService ?? throw new ArgumentNullException(nameof(userValidationService));
        }

        public async Task<PaginatedResult<User>> GetUsers(int page = 1)
        {
            return await _userRepository.GetUsers(page);
        }

        public async Task<PaginatedResult<User>> Find(string findBy, string value, int? page = null)
        {
            if (findBy == null || value == null)
            {
                return new PaginatedResult<User>();
            }

            var validProperties = new[] {"name", "email", "gender", "status"};
            findBy = findBy.Trim();
            value = value.Trim();

            // We will only call the repository if the property to find is valid
            if (!validProperties.Contains(findBy))
            {
                return new PaginatedResult<User>();
            }

            return await _userRepository.Find(findBy, value, page);
        }

        public async Task<bool> Delete(int userId)
        {
            return await _userRepository.Delete(userId);
        }

        public async Task<User> Get(int userId)
        {
            return await _userRepository.Get(userId);
        }

        public async Task<AddEditResult> Add(User user)
        {
            var isValid = _userValidationService.IsValid(user, out var validationErrors);
            if (isValid)
            {
                await _userRepository.Add(user);
            }

            return new AddEditResult(isValid, validationErrors);
        }

        public async Task<AddEditResult> Update(User user)
        {
            var isValid = _userValidationService.IsValidForEdit(user, out var validationErrors);
            if (isValid)
            {
                await _userRepository.Update(user);
            }
            return new AddEditResult(isValid, validationErrors);
        }
    }
}