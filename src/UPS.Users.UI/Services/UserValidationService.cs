﻿using System;
using System.Collections.Generic;
using System.Linq;
using UPS.Users.UI.Models;
using UPS.Users.UI.Services.Abstractions;

namespace UPS.Users.UI.Services
{
    public class UserValidationService : IUserValidationService
    {
        private readonly string[] _validGenders = {"Male", "Female"};
        private readonly string[] _validStatuses = {"Active", "Inactive"};

        public bool IsValid(User user, out IEnumerable<ValidationError> validationErrors)
        {
            var errors = new List<ValidationError>();

            NormalizeUser(user);

            if (string.IsNullOrWhiteSpace(user.Name))
            {
                errors.Add(new ValidationError(nameof(User.Name)));
            }

            ValidateEmail(user, errors);

            if (!_validGenders.Contains(user.Gender))
            {
                errors.Add(new ValidationError(nameof(User.Gender)));
            }

            if (!_validStatuses.Contains(user.Status))
            {
                errors.Add(new ValidationError(nameof(User.Status)));
            }

            validationErrors = errors;
            return !errors.Any();
        }

        public bool IsValidForEdit(User user, out IEnumerable<ValidationError> validationErrors)
        {
            var isValid = IsValid(user, out validationErrors);
            if (user.Id != default)
            {
                return isValid;
            }
            
            var errors = validationErrors.ToList();
            errors.Add(new ValidationError(nameof(User.Id)));
            validationErrors = errors;

            return false;
        }

        private void ValidateEmail(User user, List<ValidationError> errors)
        {
            try
            {
                var mailAddress = new System.Net.Mail.MailAddress(user.Email);
                if (mailAddress.Address != user.Email)
                {
                    errors.Add(new ValidationError(nameof(User.Email)));
                }
            }
            catch (Exception)
            {
                errors.Add(new ValidationError(nameof(User.Email)));
            }
        }

        private static void NormalizeUser(User user)
        {
            if (user.Name != null)
            {
                user.Name = user.Name.Trim();
            }

            if (user.Email != null)
            {
                user.Email = user.Email.Trim();
            }

            if (user.Gender != null)
            {
                user.Gender = user.Gender.Trim();
            }

            if (user.Status != null)
            {
                user.Status = user.Status.Trim();
            }
        }
    }
}