﻿namespace UPS.Users.UI.Models
{
    public class ValidationError
    {
        public string Property { get; }
        public string Value { get; }

        public ValidationError(string property)
        {
            Property = property;
            Value = $"Incorrect {property}";
        }
    }
}