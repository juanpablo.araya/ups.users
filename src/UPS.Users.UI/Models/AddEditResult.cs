﻿using System.Collections.Generic;

namespace UPS.Users.UI.Models
{
    public class AddEditResult
    {
        public bool Success { get; }
        public IEnumerable<ValidationError> ValidationErrors { get; }

        public AddEditResult(bool result, IEnumerable<ValidationError> validationErrors)
        {
            Success = result;
            ValidationErrors = validationErrors;
        }
    }
}