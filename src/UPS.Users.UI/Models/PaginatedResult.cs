﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UPS.Users.UI.Models
{
    public class PaginatedResult<T>
    {
        public int Limit { get; }
        public int Page { get; }
        public int Pages { get; }
        public int Total { get; }
        public IEnumerable<T> Data { get; }

        public PaginatedResult(
            int total,
            int pages,
            int page,
            int limit,
            IEnumerable<T> data)
        {
            Total = total;
            Pages = pages;
            Page = page;
            Limit = limit;
            Data = data ?? throw new ArgumentNullException(nameof(data));
        }

        public PaginatedResult()
        {
            Data = Enumerable.Empty<T>();
        }
    }
}