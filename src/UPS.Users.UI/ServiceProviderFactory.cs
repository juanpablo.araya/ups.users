﻿using Microsoft.Extensions.DependencyInjection;

namespace UPS.Users.UI
{
    public static class ServiceProviderFactory
    {
        public static ServiceProvider ServiceProvider { get; set; }
    }
}