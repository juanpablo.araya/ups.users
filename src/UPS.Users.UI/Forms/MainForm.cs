﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Extensions.DependencyInjection;
using UPS.Users.UI.Models;
using UPS.Users.UI.Services.Abstractions;

namespace UPS.Users.UI.Forms
{
    public partial class MainForm : Form
    {
        private readonly IUserService _userService;
        private PaginatedResult<User> _result;
        private ExecutionType _executionType;

        private enum ExecutionType
        {
            Undefined,
            Search,
            List
        };

        public MainForm(IUserService userService)
        {
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
            InitializeComponent();
            dataGridViewUsers.AutoGenerateColumns = true;
        }

        private async void MainForm_Load(object sender, EventArgs e)
        {
            _executionType = ExecutionType.List;
            await GetUsers(1);
        }

        private void buttonAddUser_Click(object sender, EventArgs e)
        {
            var form = ServiceProviderFactory.ServiceProvider.GetRequiredService<AddEditUser>();
            form.StartPosition = FormStartPosition.CenterParent;
            if (form.ShowDialog() == DialogResult.OK)
            {
                buttonRefresh_Click(sender, e);
            }
        }

        private async void buttonRefresh_Click(object sender, EventArgs e)
        {
            await GetUsers(_result.Page);
        }

        private async void buttonFirstPage_Click(object sender, EventArgs e)
        {
            if(_result.Page == 1)
            {
                return;
            }

            await GetUsers(1);
        }

        private async void buttonPreviousPage_Click(object sender, EventArgs e)
        {
            if (_result.Page == 1)
            {
                return;
            }

            await GetUsers(_result.Page - 1);
        }

        private async void buttonNextPage_Click(object sender, EventArgs e)
        {
            if(_result.Page >= _result.Pages)
            {
                return;
            }

            await GetUsers(_result.Page + 1);
        }

        private async void buttonLastPage_Click(object sender, EventArgs e)
        {
            if(_result.Page >= _result.Pages)
            {
                return;
            }

            await GetUsers(_result.Pages);
        }

        private async void buttonExport_Click(object sender, EventArgs e)
        {
            buttonExport.Enabled = false;
            var sb = new StringBuilder("Id;Name;Email;Gender;Status;CreatedAt;UpdatedAt").AppendLine();
            foreach (var user in _result.Data)
            {
                sb.AppendLine(
                    $"{user.Id};{user.Name};{user.Email};{user.Gender};{user.Status};{user.CreatedAt};{user.UpdatedAt}");
            }

            var saveFileDialog = new SaveFileDialog
            {
                Filter = "CSV File | *.csv",
                Title = "Export to CSV"
            };
            saveFileDialog.ShowDialog();

            if (saveFileDialog.FileName == "")
                return;

            using var fs = (FileStream) saveFileDialog.OpenFile();
            var data = Encoding.UTF8.GetBytes(sb.ToString());
            await fs.WriteAsync(data, 0, data.Length);
            buttonExport.Enabled = true;
        }

        private async void dataGridViewUsers_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex < 0)
            {
                return;
            }

            var selectedUserId = _result.Data.ToList()[e.RowIndex].Id;
            var user = await _userService.Get(selectedUserId);

            var form = ServiceProviderFactory.ServiceProvider.GetRequiredService<AddEditUser>();
            form.SetUser(user);
            form.StartPosition = FormStartPosition.CenterParent;
            if(form.ShowDialog() == DialogResult.OK)
            {
                buttonRefresh_Click(sender, e);
            }
        }

        private async void buttonSearch_Click(object sender, EventArgs e)
        {
            var filterBy = comboBoxSearchCriteria.SelectedItem?.ToString()?.ToLower();
            var value = textBoxSearch.Text;
            if (string.IsNullOrWhiteSpace(filterBy) || string.IsNullOrWhiteSpace(value))
            {
                return;
            }

            _executionType = ExecutionType.Search;
            await GetUsers();
        }

        private void buttonClearSearch_Click(object sender, EventArgs e)
        {
            _executionType = ExecutionType.List;
            textBoxSearch.Text = "";
            buttonRefresh_Click(sender, e);
        }

        private async Task GetUsers(int? page = null)
        {
            if (_executionType == ExecutionType.Search)
            {
                var filterBy = comboBoxSearchCriteria.SelectedItem?.ToString()?.ToLower();
                var value = textBoxSearch.Text;

                _result = await _userService.Find(filterBy, textBoxSearch.Text, page);
            }
            else
            {
                page ??= 1;
                _executionType = ExecutionType.List;
                _result = await _userService.GetUsers(page.Value);
            }

            UpdateResultInformation();
        }

        private void UpdateResultInformation()
        {
            labelTotalUsers.Text = $"{_result.Total} Users";
            labelPageInformation.Text = $"Page {_result.Page} of {_result.Pages}";
            bindingSource.DataSource = _result.Data;
            buttonFirstPage.Enabled = _result.Page > 1;
            buttonPreviousPage.Enabled = _result.Page > 1;
            buttonNextPage.Enabled = _result.Page < _result.Pages;
            buttonLastPage.Enabled = _result.Page < _result.Pages;
        }
    }
}