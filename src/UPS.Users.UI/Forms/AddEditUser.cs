﻿using System;
using System.Windows.Forms;
using UPS.Users.UI.Models;
using UPS.Users.UI.Services.Abstractions;

namespace UPS.Users.UI.Forms
{
    public partial class AddEditUser : Form
    {
        private readonly IUserService _userService;
        private User User { get; set; }

        public AddEditUser(IUserService userService)
        {
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
            InitializeComponent();
            User = new User();
            SetBindings();
        }

        public void SetUser(User user)
        {
            User = user;
            Text = "Modify User";
            buttonDelete.Visible = true;
            SetBindings();
        }

        private void SetBindings()
        {
            textboxName.DataBindings.Clear();
            textboxName.DataBindings.Add("Text", User, "Name");

            textBoxEmail.DataBindings.Clear();
            textBoxEmail.DataBindings.Add("Text", User, "Email");

            comboBoxGender.DataBindings.Clear();
            comboBoxGender.DataBindings.Add("SelectedItem", User, "Gender");

            comboBoxStatus.DataBindings.Clear();
            comboBoxStatus.DataBindings.Add("SelectedItem", User, "Status");
        }

        private async void buttonSave_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            AddEditResult result = null;
            DisableButtons();
            if (User.Id == 0)
            {
                result = await _userService.Add(User);
            }
            else
            {
                result = await _userService.Update(User);
            }

            if (result.Success)
            {
                Close();
                DialogResult = DialogResult.OK;
            }
            else
            {
                foreach (var item in result.ValidationErrors)
                {
                    SetValidationError(item);
                }
                EnableButtons();
            }
        }

        private void SetValidationError(ValidationError item)
        {
            switch (item.Property)
            {
                case nameof(Models.User.Name):
                    errorProvider1.SetError(textboxName, item.Value);
                    break;
                case nameof(Models.User.Email):
                    errorProvider1.SetError(textBoxEmail, item.Value);
                    break;
                case nameof(Models.User.Gender):
                    errorProvider1.SetError(comboBoxGender, item.Value);
                    break;
                case nameof(Models.User.Status):
                    errorProvider1.SetError(comboBoxStatus, item.Value);
                    break;
            }
        }

        private async void buttonDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete the user?", "Delete", MessageBoxButtons.YesNo) ==
                DialogResult.Yes)
            {
                DisableButtons();
                await _userService.Delete(User.Id);
                Close();
                DialogResult = DialogResult.OK;
            }
            else
            {
                EnableButtons();
            }
        }

        private void DisableButtons()
        {
            EnableDisableButtons(false);
        }

        private void EnableButtons()
        {
            EnableDisableButtons(true);
        }

        private void EnableDisableButtons(bool enable)
        {
            buttonSave.Enabled = enable;
            buttonDelete.Enabled = enable;
        }
    }
}