﻿using System.Threading.Tasks;
using UPS.Users.UI.Models;

namespace UPS.Users.UI.Repositories.Abstractions
{
    public interface IUserRepository
    {
        Task<PaginatedResult<User>> GetUsers(int page);
        Task<PaginatedResult<User>> Find(string property, string value, int? page);
        Task<bool> Delete(int userId);
        Task<User> Get(int userId);
        Task Add(User user);
        Task Update(User user);
    }
}