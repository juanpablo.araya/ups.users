﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using UPS.Users.UI.Models;
using UPS.Users.UI.Repositories.Abstractions;

namespace UPS.Users.UI.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly ApiSettings _apiSettings;

        public UserRepository(ApiSettings apiSettings)
        {
            _apiSettings = apiSettings ?? throw new ArgumentNullException(nameof(apiSettings));
        }

        public async Task<PaginatedResult<User>> GetUsers(int page)
        {
            return await Get($"{_apiSettings.Url}?page={page}");
        }

        public async Task<PaginatedResult<User>> Find(string property, string value, int? page = null)
        {
            var sb = new StringBuilder($"{_apiSettings.Url}?{property}={value}");
            if (page.HasValue)
            {
                sb.Append($"&page={page}");
            }
            return await Get(sb.ToString());
        }

        private async Task<PaginatedResult<User>> Get(string url)
        {
            using var httpClient = new HttpClient();
            using var request = new HttpRequestMessage(HttpMethod.Get, new Uri(url));
            using var response = await httpClient.SendAsync(request);

            if (!response.IsSuccessStatusCode)
            {
                return new PaginatedResult<User>();
            }

            var responseData = await response.Content.ReadAsStringAsync();
            var json = JObject.Parse(responseData);
            var metaPagination = json["meta"]["pagination"];
            return new PaginatedResult<User>(
                metaPagination["total"].ToObject<int>(),
                metaPagination["pages"].ToObject<int>(),
                metaPagination["page"].ToObject<int>(),
                metaPagination["limit"].ToObject<int>(),
                json["data"].ToObject<User[]>());
        }

        public async Task<bool> Delete(int userId)
        {
            using var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", _apiSettings.ApiToken);
            using var request = new HttpRequestMessage(HttpMethod.Delete, new Uri($"{_apiSettings.Url}/{userId}"));
            using var response = await httpClient.SendAsync(request);
            return response.IsSuccessStatusCode;
        }

        public async Task<User> Get(int userId)
        {
            using var httpClient = new HttpClient();
            using var request = new HttpRequestMessage(HttpMethod.Get, new Uri($"{_apiSettings.Url}/{userId}"));
            using var response = await httpClient.SendAsync(request);

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var responseData = await response.Content.ReadAsStringAsync();
            var json = JObject.Parse(responseData);
            return json["data"].ToObject<User>();
        }

        public async Task Add(User user)
        {
            using var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", _apiSettings.ApiToken);

            var json = new JObject();
            json["name"] = user.Name;
            json["email"] = user.Email;
            json["gender"] = user.Gender;
            json["status"] = user.Status;
            var buffer = Encoding.UTF8.GetBytes(json.ToString());
            using var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            await httpClient.PostAsync(new Uri($"{_apiSettings.Url}"), byteContent);
        }

        public async Task Update(User user)
        {
            using var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", _apiSettings.ApiToken);

            var json = new JObject
            {
                ["id"] = user.Id,
                ["name"] = user.Name,
                ["email"] = user.Email,
                ["gender"] = user.Gender,
                ["status"] = user.Status
            };
            var buffer = Encoding.UTF8.GetBytes(json.ToString());
            using var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            await httpClient.PatchAsync(new Uri($"{_apiSettings.Url}/{user.Id}"), byteContent);
        }
    }
}