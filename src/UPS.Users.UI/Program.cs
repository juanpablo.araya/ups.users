using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UPS.Users.UI.Forms;
using UPS.Users.UI.Repositories;
using UPS.Users.UI.Repositories.Abstractions;
using UPS.Users.UI.Services;
using UPS.Users.UI.Services.Abstractions;

namespace UPS.Users.UI
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var apiSettings = new ApiSettings();
            GetConfiguration(apiSettings);

            var services = new ServiceCollection();
            ConfigureServices(services, apiSettings);

            using var serviceProvider = services.BuildServiceProvider();
            ServiceProviderFactory.ServiceProvider = serviceProvider;
            var form = serviceProvider.GetRequiredService<MainForm>();
            Application.Run(form);
        }

        private static void GetConfiguration(ApiSettings apiSettings)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true)
                .Build();
            ConfigurationBinder.Bind(configuration.GetSection("apiSettings"), apiSettings);
        }

        private static void ConfigureServices(ServiceCollection services, ApiSettings apiSettings)
        {
            services.AddScoped<IUserValidationService, UserValidationService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<MainForm>();
            services.AddTransient<AddEditUser>();
            services.AddSingleton<ApiSettings>(apiSettings);
        }
    }
}