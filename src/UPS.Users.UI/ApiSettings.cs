﻿namespace UPS.Users.UI
{
    public class ApiSettings
    {
        public string Url { get; set; }
        public string ApiToken { get; set; }
    }
}